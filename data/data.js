var EVENT = {
    "name": "Expedice Spolu",
    "date": "Pravděpodobně Březen 2021",
    "image": "data/Panorama.jpg",
    "about": [
        {
            "title": "Otec a syn",
            "motto": "\"Moc rád vzpomínám, jak mě táta vzal poprvé na výlet do Klokočích skal.\"",
            "text": "Máte podobné vzpomínky? Pojďme tentokrát my vzít tátu na mužský výlet do zasněžených Alp.",
            "image": "data/FatherAndSon1.jpg"
        },
        {
            "title": "Na sněžnicích",
            "text": "Pohybovat se budeme horskou krajinou, kde se bez sněžnic nebo skialpových lyží neobejdeme. Program není svoji náročností nijak kritický, přesto může být pro některé výzvou.",
            "image": "data/SnowshoesSign.jpg"
        },
        {
            "title": "Liezener Hütte",
            "text": "Přespání 2 noci ve vlastním spacáku na krásné horské chatě bez obsluhy. Dřevěná chata je vybavena několika pokoji s palandami, kuchyní s kamny a základním nádobím. Před chalupou pramení pitná voda a v místní (vypnuté) lednici čeká pivo a víno. Platí se před odchodem v hotovosti do kasičky.",
            "image": "data/LiezenerHutte.jpg"
        }
    ],
    "program": [
        {
            "title": "Čtvrtek",
            "description": "Večerní přejezd do Lince/Liezenu, tak abychom v pátek dopoledne mohli včas zahájit výstup. Přesný čas a místo srazu a prvního přespání ještě upřesníme.",
            //"map": "https://frame.mapy.cz/s/memulujoda"
        },
        {
            "title": "Pátek",
            "description": "Přejezd do Wörschachu na parkoviště v 1000 m.n.m. Výstup cca. 700 výškových metrů na chatu Liezener Hütte."
        },
        {
            "title": "Sobota",
            "description": "Výlet na vrchol Hochmölbing 2336 m. n. m. s převýšením asi 600 metrů."
        },
        {
            "title": "Neděle",
            "description": "Sestup na parkoviště a cesta domů."
        }
    ],
    "organization" :{
        "date": {
            "title": "Pravděpodobně únor 2021 - podle situace"
        },
        "journey": {
            "title": "Spolujízdou auty",
            "link": "https://docs.google.com/spreadsheets/d/1Um-zA2Lue2wqaGEDMwPhZ9UCIP8mLAI5LXCq5RA4S2A/edit?usp=sharing"
        },
        "price": {
            "title": "Nocleh (celkem cca. €80 na osobu), jídlo, doprava, půjčení vybavení"
        }
    },
    "equipment": [
        "Sněžice",
        "Hůlky",
        "Lavinová výbava (pípák-sonda-lopata)",
        "Spacák (do chalupy)",
        "Čelovka",
        "Dostatek oblečení",
        "Přezůvky",
        "Pití (alespoň 2 litry)",
        "Jídlo (vaření na kamnech, základní nádobí na chalupě)",
        "Hudební nástroj (kdo unese)",
        "Nabitý telefon",
        "Pojištění a doklady",
        "Zabaleno do batohu"],
    "map": "https://frame.mapy.cz/s/nebunofofe",
    "links": [
        {
            "url": "https://www.liezener-huette.at/",
            "title": "Chata Liezener Hütte"
        },
        {
            "url": "https://www.oeav.cz/pujcovna/lavinova-vystroj-sneznice",
            "title": "OEAV - půjčovna vybavení"
        }
    ],
    "contact": {
        "name": "Jára Deák",
        "phone": "+420 775 617 695",
        "email": "jaromir.deak@gmail.com"
    }
};