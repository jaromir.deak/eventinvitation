# EventInvitation

Jednoduchá šablona pro vytvoření pozvánky na různé akce nebo výlety.

# Jak začít?

Celý obsah pozvánky lze upravit v souboru data/data.js, kde jsou včechny informace ve formátu JSON.

Do pozvánky lze připojit obrázky ze složky data.

Sekce s mapou akceptuje odkazy vytvořené v aplikaci mapy.cz.

-------------------------------------------------------------------------------

Simple web template to create easy invitation for various events and trips.

# Get started

Modifying JSON described content in data/data.js changes the result webpage.

Images added to data folder can be linked.

The template supports map frames loaded from https://mapy.cz/.
